package sheridan;

import static org.junit.Assert.*;
import sheridan.MealType;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class MealsServiceTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		
	}
	@After
	public void tearDown() throws Exception {
	}

	//Regular
	@Test
	public void testDrinksRegular() {
		MealsService ms = new MealsService();
		assertTrue("List is empty", ms.getAvailableMealTypes(MealType.DRINKS).size() != 0);
	}
	// Exception
	@Test
	public void testDrinksException() {
		MealsService ms = new MealsService();
		assertFalse("First item is not null",  ms.getAvailableMealTypes(null).equals("No Brands Available"));
	}
	
	// Boundary In
	@Test
	public void testDrinksBoundaryIn() {
		MealsService ms = new MealsService();
		assertTrue("List is not larger than 3", ms.getAvailableMealTypes(MealType.DRINKS).size() > 3);
	}
	
	// Boundary Out
	@Test
	public void testDrinksBoundaryOut() {
		MealsService ms = new MealsService();
		assertTrue("First item is not null",  ms.getAvailableMealTypes(null).size() == 1);
	}

}
